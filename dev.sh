#!/usr/bin/env bash

PROJECT_NAME=pool-party-dev

if [ "$1" == "up" ]; then

  # start setup
  docker-compose -f docker-compose-dev.yaml --project-name ${PROJECT_NAME} up -d --force-recreate
  docker-compose -f docker-compose-dev.yaml --project-name ${PROJECT_NAME} logs -f

elif [ "$1" == "setup" ]; then
  #git submodule update --recursive --remote

  docker-compose -f docker-compose-dev.yaml --project-name ${PROJECT_NAME} up -d --force-recreate --build
  docker-compose -f docker-compose-dev.yaml --project-name ${PROJECT_NAME} run pool-manager rails db:create
  docker-compose -f docker-compose-dev.yaml --project-name ${PROJECT_NAME} run donation-manager rails db:create
  docker-compose -f docker-compose-dev.yaml --project-name ${PROJECT_NAME} logs -f

elif [ "$1" == "db:create" ]; then

  docker-compose -f docker-compose-dev.yaml run pool-manager rails db:create
  docker-compose -f docker-compose-dev.yaml run donation-manager rails db:create

elif [ "$1" == "logs" ]; then

    docker-compose --project-name ${PROJECT_NAME} logs -f

elif [ "$1" == "stop" ]; then

    docker-compose --project-name ${PROJECT_NAME} stop

elif [ "$1" == "down" ]; then

    docker-compose --project-name ${PROJECT_NAME} down -v

elif [ "$1" == "update" ]; then

    git submodule update --recursive --remote

fi
