# Pool Pary
IMPORTANT: Please clone this repository with a recursive strategy in order to fetch all submodules as well.
```bash
git clone --recursive https://gitlab.com/einfachiota/pool-party
```

## Setup

Please execute for the first time

```bash
./pool.sh create
```

after that, you can start the pool party with

```bash
./pool.sh up
```

### Prerequisites:

- Docker
- Docker Compose
