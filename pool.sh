#!/usr/bin/env bash

PROJECT_NAME=pool-party

if [ "$1" == "up" ]; then

  # start setup
  docker-compose --project-name ${PROJECT_NAME} up -d --force-recreate
  docker-compose --project-name ${PROJECT_NAME} logs -f

elif [ "$1" == "setup" ]; then

  docker-compose --project-name ${PROJECT_NAME} up -d --force-recreate
  docker-compose --project-name ${PROJECT_NAME} run pool-manager rails db:create
  docker-compose --project-name ${PROJECT_NAME} run donation-manager rails db:create
  docker-compose --project-name ${PROJECT_NAME} logs -f

elif [ "$1" == "console-pool" || "$1" == "c-pool" ]; then

    docker-compose --project-name ${PROJECT_NAME} run pool-manager rails c

elif [ "$1" == "console-donation" || "$1" == "c-donation"  ]; then

  docker-compose --project-name ${PROJECT_NAME} run donation-manager rails c

elif [ "$1" == "logs" ]; then

    docker-compose --project-name ${PROJECT_NAME} logs -f

elif [ "$1" == "stop" ]; then

    docker-compose --project-name ${PROJECT_NAME} stop

elif [ "$1" == "down" ]; then

    docker-compose --project-name ${PROJECT_NAME} down -v

elif [ "$1" == "update" ]; then
    git pull
    docker-compose pull
fi
